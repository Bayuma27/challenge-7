const winner = (p1, p2) => {
    if (
      (p1 === "batu" && p2 === "batu") ||
      (p1 === "kertas" && p2 === "kertas") ||
      (p1 === "gunting" && p2 === "gunting")
    ) {
      (p1Final = "draw"), (p2Final = "draw");
    } else if (
      (p1 === "batu" && p2 === "gunting") ||
      (p1 === "kertas" && p2 === "batu") ||
      (p1 === "gunting" && p2 === "kertas")
    ) {
      (p1Final = "win"), (p2Final = "lose");
    } else if (
      (p1 === "batu" && p2 === "kertas") ||
      (p1 === "kertas" && p2 === "gunting") ||
      (p1 === "gunting" && p2 === "batu")
    ) {
      (p1Final = "lose"), (p2Final = "win");
    }
    return [p1Final, p2Final];
  };
  
  const comChoice = () => {
    const choice = ["batu", "kertas", "gunting"];
    const random = Math.floor(Math.random() * choice.length);
    return choice[random];
  };
  
  module.exports = { winner, comChoice };
  