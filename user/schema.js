const { body } = require("express-validator");

const schemaRegistrasi = [
    body("username")
        .notEmpty().withMessage("Username tidak boleh kosong :)"),

    body("email")
        .notEmpty()
        .withMessage("Mohon isikan email terlebih dahulu :)")
        .isEmail()
        .withMessage("Salah nih format emailnya :)"),

    body("password")
        .notEmpty()
        .withMessage("Mohon isikan password terlebih dahulu")
        .isLength({ min: 6 })
        .withMessage("Password minimal 6 character"),
];

const schemaLogin = [
    body("username")
        .notEmpty()
        .withMessage("Username wajib diisi")
        .isString()
        .withMessage("Format username harus berupa huruf"),

    body("email")
        .notEmpty()
        .withMessage("Mohon isikan email terlebih dahulu :)")
        .isEmail()
        .withMessage("Salah nih format emailnya :)"),

    body("password")
        .notEmpty()
        .withMessage("Mohon isikan password terlebih dahulu")
        .isLength({ min: 6 })
        .withMessage("Password minimal 6 character"),
];

const schemaUpdateBio = [
    body("fullname")
        .notEmpty()
        .withMessage("Isikan nama lengkapmu")
        .isString(),
    body("address")
        .notEmpty()
        .withMessage("Isikan alamat rumahmu terlebih dahulu")
        .isString()
        .withMessage("Format input salah!"),
    body("phoneNumber")
        .notEmpty()
        .withMessage("Masukkan nomor telephone terlebih dahulu :)")
        .isMobilePhone("id-ID")
        .withMessage("Format nomor telphone salah"),
    body("age")
        .notEmpty()
        .withMessage("Isikan umur anda"),
];

const schemaInputHistory = [
    body("pilihan").notEmpty().withMessage("Select your choice"),
    body("judgement")
        .notEmpty()
        .withMessage("Tolong masukan pilihan kamu")
        .isString()
        .withMessage("Format input salah, harus menggunakan huruf"),
];


module.exports = {
    schemaRegistrasi,
    schemaLogin,
    schemaUpdateBio,
    schemaInputHistory,
}