const md5 = require("md5");
const db = require('../db/models');
const { Op } = require("sequelize");

const userList = [];

class userModel {
  //Mendapatkan semua data
  getAllusers = async () => {
    const allUsers = await db.User.findAll({ include: [db.UserBio, db.GameHistory], });
    return allUsers;
  };

  //Mendapatkan single user
  getSingleUser = async (idUser) => {
    return await db.User.findOne({ include: [db.UserBio, db.GameHistory], where: { id: idUser } });
  };

  //Menambahkan data baru
  recordNewData = (dataRequest) => {
    console.log(dataRequest);
    db.User.create({
      username: dataRequest.username,
      email: dataRequest.email,
      password: md5(dataRequest.password),
    });
  };

  // method registrasi username
  isUserRegistered = async (dataRequest) => {
    const existData = await db.User.findOne({
      where: {
        [Op.or]: [
          { username: dataRequest.username },
          { email: dataRequest.email },
        ],
      },
    });

    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  //Login
  userLogin = async (username, email, password) => {
    const LoginUser = await db.User.findOne({
      where: {
        username: username,
        email: email,
        password: md5(password)
      },
      attributes: { exclude: ['password'] },
      raw: true,
    });

    return LoginUser;
  };

  //Mengupdate UserBio
  updateUserBio = async (idUser, fullname, address, phoneNumber, age) => {
    return await db.UserBio.update(
      {
        fullname: fullname,
        address: address,
        phoneNumber: phoneNumber,
        age: age
      },
      { where: { userId: idUser } }
    );
  };

  //memastikan user bio sudah ada
  isUserBioExist = async (idUser) => {
    const existDataBio = await db.UserBio.findOne({
      where: {
        userId: idUser,
      },
    });

    if (existDataBio) {
      return true;
    } else {
      return false;
    }
  };

  //menambahkan user bio jika belum ada user bio
  createUserBio = async (idUser, fullname, address, phoneNumber, age) => {
    return await db.UserBio.create({
      fullname,
      address,
      phoneNumber,
      age,
      userId: idUser,
    });
  };

  //Menambahkan History Game
  recordGame = (dataRequestGame) => {
    console.log(dataRequestGame);
    db.GameHistory.create({
      userId: dataRequestGame.userId,
      status: dataRequestGame.status,
      time: dataRequestGame.time,
    });
  }

  //Mendapatkan History Game
  getGameHistory = async (idUser) => {
    return await db.GameHistory.findOne({ include: [db.User], where: { id: idUser } });
  };

  // ------------------ Game model penunjang API baru neh ------------------ 

  // Membuat room game
  createGameRoom = (player1, token) => {
    db.gameRoom.create({
      namaRoom: player1.namaRoom,
      player1Id: token,
      player1Choice: player1.player1Choice,
      statusRoom: "Available",
    });
  };

  // Join game Room
  joinGameRoom = (player2, token, idUser) => {
    const roomId = parseInt(idUser);
    db.gameRoom.update(
      {
        player2Choice: player2.playerTwoChoice,
        player2Id: token,
      },
      { where: { idUser: roomId } }
    );
  };

  // Mendapatkan semua data Room
  getAllRooms = async () => {
    const Rooms = await db.gameRoom.findAll({
      include: [
        {
          model: db.User,
          as: "player1",
          attributes: ["id", "username"],
        },
        {
          model: db.User,
          as: "player2",
          attributes: ["id", "username"],
        },
      ],
    });
    return Rooms;
  };

  // Mendapatkan detail salah satu Room
  getSingleRoom = async (id) => {
    const roomId = parseInt(id);
    const singleRoom = await db.gameRoom.findOne({
      where: { id: roomId },

      include: [
        {
          model: db.user,
          as: "player1",
        },
        {
          model: db.user,
          as: "player2",
        },
      ],
      raw: true,
    });
    return singleRoom;
  };

  // Mendapatkan Game User history
  getUserHistory = async (id) => {
    try {
      const roomList = await db.gameRoom.findAll({
        attributes: [
          "namaRoom",
          "updatedAt",
          "player1Id",
          "player2Id",
          "player1Result",
          "player2Result",
        ],
        where: {
          [Op.or]: [{ player1Id: idRoom }, { player2Id: idRoom }],
          [Op.and]: [
            { player1Result: { [Op.ne]: null } },
            { player2Result: { [Op.ne]: null } },
          ],
        },
        raw: true,
      });

      return roomList.map((room) => ({
        player_id: room.player1_id === id ? room.player1_id : room.player2_id,
        hasilPlayer:
          room.player1_id === id ? room.player1_result : room.player2_result,
        roomName: room.nama_room,
        updatedAt: room.updatedAt,
      }));
    } catch (error) {
      throw new Error("Gagal menerima room detail : " + error.message);
    }
  };

  // Membuat game Player melawan Computer
  createPlayerVsCom = async (
    namaRoom,
    idRoom,
    player1Choice,
    player1Result,
    player2Choice,
    player2Result
  ) => {
    try {
      return await db.gameRoom.create({
        namaRoom,
        player1Id: idRoom,
        player1Choice,
        player1Result,
        player2Choice,
        player2Result,
        statusRoom: "Finish",
      });
    } catch (error) {
      console.log(error);
    }
  };

}

module.exports = new userModel();