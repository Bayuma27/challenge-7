const userModel = require('./model');
const jwtTool = require("jsonwebtoken");
const { validationResult } = require('express-validator');
const winner = require("../utils/winnerValidation");

class UserController {
  //Mendapatkan semua data User
  getAlluser = async (req, res) => {
    try {
      const allUsers = await userModel.getAllusers();
      return res.json(allUsers);
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "Terjadi Kesalahan" })
    }
  };

  //Mendapatkan detail user menggunakan id user
  getSingleUser = async (req, res) => {
    const { idUser } = req.params;
    try {
      const users = await userModel.getSingleUser(idUser);
      if (users) {
        return res.json(users);
      } else {
        res.statusCode = 400;
        return res.json({ message: `User id tidak ditemukan : ${idUser}` });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: `User id tidak ditemukan : ${idUser}` });
    }
  };

  //Registrasi User
  registrasiUser = async (req, res) => {
    const dataRequest = req.body;
    try {
      const existData = await userModel.isUserRegistered(dataRequest);
      if (existData) {
        return res.json({ message: "Username or Email is exist!" });
      }
      userModel.recordNewData(dataRequest);
      return res.json({ message: "New user is recorded" });
    } catch (error) {
      res.statusCode = 400;
      console.log(error);
      return res.json({ message: "Terjadi kesalahan nih :)" })
    }
  };

  //Login User
  loginUser = async (req, res) => {
    const { username, email, password } = req.body;
    try {
      const dataLogin = await userModel.userLogin(username, email, password);
      // kalau user valid
      if (dataLogin) {
        // generate jwt
        const token = jwtTool.sign(
          { ...dataLogin, role: "Player" },
          "jakjfhkjadkfj*&^",
          { expiresIn: "1d" }
        );

        return res.json({ accessToken: token });
      } else {
        return res.json({ message: "Invalid credential" });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "User tidak ditemukan" })
    }
  };

  //Update & create User Bio berdasarkan ID
  updateUserBio = async (req, res) => {
    const { idUser } = req.params;
    const { fullname, address, phoneNumber, age } = req.body;
    const existUser = await userModel.isUserBioExist(idUser);
    try {
      if (existUser) {
        //update user bio jika sudah ada data user bio
        userModel.updateUserBio(
          idUser,
          fullname,
          address,
          phoneNumber,
          age
        );
        return res.json({ message: "User Bio is Updated!" });
      } else {
        //buat user bio jika sudah ada data user bio
        const data = await userModel.createUserBio(
          idUser,
          fullname,
          address,
          phoneNumber,
          age
        );
        return res.json({ message: "User Bio is Created!" });
      }
    } catch (error) {
      res.statusCode = 400;
      res.json({ message: "Terjadi Kesalahan" })
    }
  };

  //Record History Game
  insertGame = async (req, res) => {
    const dataRequestGame = req.body;
    const existGame = await userModel.recordGame(dataRequestGame);
    //Menambahkan data baru
    // userModel.recordGame(dataRequestGame);
    return res.json({ message: "New Game History Is Recorded" });
  };

  //Mendapatkan history permainan berdasarkan ID
  getHistory = async (req, res) => {
    const { idUser } = req.params;
    try {
      const gamehis = await userModel.getGameHistory(idUser);
      console.log(gamehis);
      if (gamehis) {
        return res.json(gamehis);
      } else {
        res.statusCode = 400;
        return res.json({ message: `User id tidak ditemukan : ${idUser}` });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "Ada error nih :)" });
    }
  };

  // --------------- API Game baru nih ---------------

  // Membuat room game
  createGameRoom = async (req, res) => {
    const player1 = req.body;
    const token = req.token.id;

    try {
      if (
        player1.player1Choice !== "batu" &&
        player1.player1Choice !== "kertas" &&
        player1.player1Choice !== "gunting"
      ) {
        return res
          .status(400)
          .json({ message: "Format pilihan harus berupa batu / kertas / gunting" });
      }

      await userModel.createGameRoom(player1, token);
      return res.json({ message: "Room is Created :D" });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "Wah ada Kesalahan :(" });
    }
  };

  // masuk ke room game
  joinGameRoom = async (req, res) => {
    const player2 = req.body;
    const token = req.token.id;
    const { idUser } = req.params;

    try {
      // cari game room berdasarkan id
      const singleRoom = await userModel.getSingleRoom(idUser);

      // player vs other
      if (singleRoom.player1Id === token) {
        res.statusCode = 400;
        return res.json({ message: "Select other Enemy :D" });
      }

      // Permainan hanya dilakukan sekali
      if (singleRoom.statusRoom === "Finish") {
        res.statusCode = 400;
        return res.json({ message: "Have Done!" });
      }

      // input data pilihan player 2 ke database
      await userModel.joinRoom(player2, token, idUser);

      const ResultP1 = singleRoom.player1Choice;
      const ResultP2 = singleRoom.player2Choice;

      // tentukan pemenang
      const result = await winner.winner(ResultP1, ResultP2);

      // masukan result pada database
      const RoomResult = await userModel.RoomResult(result, idUser);
      res.json({ message: "Congrats!" });
      return RoomResult;
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "ada yang salah di controller joinRoom" });
    }
  };

  //Mendapatkan semua Room
  getAllRooms = async (req, res) => {
    try {
      const allRooms = await userModel.getAllRooms();
      return res.json(allRooms);
    } catch (error) {
      return res
        .status(400)
        .json({ message: "Error nih :(" });
    }
  };

  // Mendapatkan room berdasarkan ID
  getSingleRoom = async (req, res) => {
    const { id } = req.params;
    try {
      const singleRoom = await userModel.getSingleRoom(id);
      return res.json(singleRoom);
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "Error nih :(" });
    }
  };

  getSingleHistory = async (req, res) => {
    const idUser = req.token.id;
    try {
      const userHistory = await userModel.getSingleHistory(idUser);
      return res.json(userHistory);
    } catch (error) {
      return res
        .status(400)
        .json({ message: "Error nih :(" });
    }
  };

  createPlayerVsCom = async (req, res) => {
    const id = req.token.id;
    const { player1Choice } = req.body;

    try {
      // create room dan rekam pilihan player 1
      const namaRoom = "VS COM";
      const player2Choice = winner.vsCom();
      const [player1Result, player2Result] = winner.winner(
        player1Choice,
        player2Choice
      );

      // buat agar inputan hanya berupa batu kertas atau gintung
      if (
        player1Choice !== "batu" &&
        player1Choice !== "kertas" &&
        player1Choice !== "gunting"
      ) {
        return res
          .status(400)
          .json({ message: "Format harus berupa batu / kertas / gunting" });
      }

      await userModel.playerVsCom(
        namaRoom,
        id,
        player1Choice,
        player1Result,
        player2Choice,
        player2Result
      );
      return res.json({ message: "berhasil membuat room playerVsCom" });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "pilihan harus berupa batu / kertas / gunting" });
    }
  };


}

module.exports = new UserController();