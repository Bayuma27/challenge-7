const express = require('express');
const userRouter = express.Router();
const userController = require('./controller');
const authMiddleware = require('../middleware/authMiddleware');
const schemaValidation = require('../middleware/validationSchema');
const validator = require('./schema');
const protection = require('../middleware/protection');

// API mendapatkan semua data User
userRouter.get(
    "/user",
    authMiddleware,
    userController.getAlluser);

// API Registrasi User menggunakan metode POST
userRouter.post(
    "/registrasi",
    validator.schemaRegistrasi,
    schemaValidation,
    userController.registrasiUser);

// API Login User menggunakan metode POST
userRouter.post(
    "/login",
    validator.schemaLogin,
    schemaValidation,
    userController.loginUser);

// API mendapatkan detail user bersdasarkan ID
userRouter.get(
    "/detail/:idUser",
    authMiddleware,
    protection,
    userController.getSingleUser);

// API mengupdate dan menambahkan user Bio(detail profil)
userRouter.put(
    "/updatebio/:idUser",
    authMiddleware,
    protection,
    validator.schemaUpdateBio,
    userController.updateUserBio);

// API menambahkan history game
userRouter.post(
    "/game",
    authMiddleware,
    // protection,
    validator.schemaInputHistory,
    userController.insertGame);

// API mendapatkan history game
userRouter.get(
    "/history/:idUser",
    protection,
    userController.getHistory);

    // ----------------- API Baru untuk Game Room --------------------

// API Create Room
userRouter.post(
    "/room",
    authMiddleware,
    userController.createGameRoom);

// API  join Room player 2
userRouter.put(
    "/joinRoom/:idRoom",
    userController.joinGameRoom
);

// API get All Rooms
userRouter.get(
    "/rooms",
    authMiddleware,
    userController.getAllRooms
);

// API get single Room
userRouter.get(
    "/room/:idRoom",
    userController.getSingleRoom
);

// API untuk mendapatkan History user
userRouter.get(
    "/history",
    userController.getSingleHistory
);

// API untuk Player vs Computer
userRouter.post(
    "/vs",
    userController.createPlayerVsCom
);

module.exports = userRouter;