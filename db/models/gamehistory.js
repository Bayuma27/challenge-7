'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      GameHistory.belongsTo(models.User, {foreignKey: "userId"});
    }
  }
  GameHistory.init({
    userId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    time: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'GameHistory',
  });
  return GameHistory;
};