const { validationResult } = require('express-validator');

const schemaValidation = async (req, res, next) => {
  const result = validationResult(req);

  if (result.isEmpty()) {
    next();
  } else {
    console.log(result);
    res.statusCode = 400;
    return res.json({ errors: result.errors });
  }
};

module.exports = schemaValidation;