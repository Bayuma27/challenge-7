const jwtTool = require("jsonwebtoken");
require("dotenv").config();

const authMiddleware = async (req, res, next) => {
    // cek ada Authorization di dalam header
    // ambil key authorization dalam header
    const { authorization } = req.headers;

    //jika authorization undefined / null maka reject request
    if (authorization === undefined) {
        res.statusCode = 400;
        return res.json({ message: "Unauthorization" });
    };

    try {
        //cek apakah authorization valid, kalau tidak valid return unauthorized
        const token = await jwtTool.verify(
            authorization,
            "jakjfhkjadkfj*&^"
        );
        req.token = token;
        next();
    } catch (error) {
        res.statusCode = 400;
        return res.json({ message: "Invalid Token" });
    }
};

module.exports = authMiddleware;