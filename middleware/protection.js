const protection = async (req, res, next) => {
  const tokenId = req.token.id.toString();
  try {
    const { idUser } = await req.params;

    if (tokenId === idUser) {
      next();
    } else {
      return res.status(400).json({ error: "Unauthorized" });
    }
  } catch (error) {
    return res.status(400).json({ message: "Ada kesalahan nih :)" });
  }
};

module.exports = protection;